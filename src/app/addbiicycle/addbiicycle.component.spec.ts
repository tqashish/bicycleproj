import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddbiicycleComponent } from './addbiicycle.component';

describe('AddbiicycleComponent', () => {
  let component: AddbiicycleComponent;
  let fixture: ComponentFixture<AddbiicycleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddbiicycleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddbiicycleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
