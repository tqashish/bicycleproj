import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { RegisrService } from '../regisr.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-addbiicycle',
  templateUrl: './addbiicycle.component.html',
  styleUrls: ['./addbiicycle.component.css']
})
export class AddbiicycleComponent implements OnInit {
  constructor(public sc:RegisrService,private r:Router) { }

  AddBicycles=new FormGroup({
    modelno: new FormControl(''),
    bicycleName: new FormControl(''),
    price:new FormControl(''),
    colors:new FormControl(''),
    weight:new FormControl(''),
    information:new FormControl(''),
    image:new FormControl(''),
  });

  selectedFile:File=null;
  



  onFileSelected(file:FileList){
    this.selectedFile=file.item(0);
    var reader=new FileReader();
  reader.onload=(event:any)=>{
  this.AddBicycles.value.image=event.target.result;
  
  }
  reader.readAsDataURL(this.selectedFile);
  
  }
  
  ngOnInit() {
  }

add(){
  this.sc.addbicycle(this.AddBicycles.value).subscribe((d=>this.my(d)))
}
my(d){
  if(d.Msg=="successfully")
  {
    alert("successfully");
  }
  else{
    alert("not successfully");
  }
  this.r.navigate(['/cycles']);
}

}
