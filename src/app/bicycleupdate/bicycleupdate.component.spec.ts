import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BicycleupdateComponent } from './bicycleupdate.component';

describe('BicycleupdateComponent', () => {
  let component: BicycleupdateComponent;
  let fixture: ComponentFixture<BicycleupdateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BicycleupdateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BicycleupdateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
