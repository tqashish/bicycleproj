import { Component, OnInit } from '@angular/core';
import { RegisrService } from '../regisr.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
@Component({
  selector: 'app-bicycleupdate',
  templateUrl: './bicycleupdate.component.html',
  styleUrls: ['./bicycleupdate.component.css']
})
export class BicycleupdateComponent implements OnInit {

  
    bicycleform: FormGroup;
    constructor(public sc:RegisrService ,private a:ActivatedRoute,private r:Router) { }
    tp:number;
    bicy: any[] = [];
  
    list:any={}; 
   
    ngOnInit() {
  
      this.tp=this.a.snapshot.params['i'];
  
      this.sc.viewbicycle().subscribe((d: any) =>
      {
        this.bicy = d
  
        for(let i=0;i<this.bicy.length;i++)
        {
          if(i==this.tp)
          {
            this.list=this.bicy[i];
            break;
          }
        }
      });
  
   
    }
  
    addbicycle()
    {
      
      this.sc.edit(this.list).subscribe(d=>
        {
           this.r.navigate(['/cycles']);
        });   
  
    }
  
  }
  



