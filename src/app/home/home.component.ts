import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor() { }
  getUrl()
  {
    return "url(https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcS8yZ5LzCT56rg7zCovWzLZ3DIe471a2Ya61vZVzzbzJyr6E6NV&s)";
  }
  ngOnInit() {
  }

}
