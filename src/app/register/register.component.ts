import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { RegisrService } from '../regisr.service';
// import { ReactiveFormsModule } from '@angular/forms';
@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {


 

  constructor(private d:RegisrService) { }

  ngOnInit() {
  }

  res:any={};
  save()
  {
    this.d.regi(this.registrationform.value).subscribe(r=>this.res=r);
  }



  registrationform=new FormGroup({
uname: new FormControl('',[Validators.required]),
gender:new FormControl('',[Validators.required]),
password: new FormControl('',[Validators.required,Validators.minLength(3)]),
mobile: new FormControl('',[Validators.required,Validators.minLength(10)]),
email: new FormControl('',[Validators.required,Validators.pattern("@[a-z]")]),
address: new FormControl('',[Validators.required])


});

  
}
