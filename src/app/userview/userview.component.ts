import { Component, OnInit } from '@angular/core';
import { RegisrService } from '../regisr.service';

@Component({
  selector: 'app-userview',
  templateUrl: './userview.component.html',
  styleUrls: ['./userview.component.css']
})
export class UserviewComponent implements OnInit {

  constructor(private v:RegisrService) { }

bicycle:any[]=[];

  ngOnInit() {

this.fetchdata();

}
fetchdata(){
this.v.viewbicycle().subscribe((b:any)=>this.bicycle=b);

  }
  cart(z)
  {
    for(let i=0;i<this.bicycle.length;i++)
    {
      if(i==z)
      {
        let arr=JSON.parse(localStorage.getItem("cart"));
        if(arr==null)
        {
          arr=[];
        }
        arr.push(this.bicycle[i]);
        localStorage.setItem("cart",JSON.stringify(arr));
      }
    }
  }
}  


