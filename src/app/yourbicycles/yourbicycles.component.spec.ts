import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YourbicyclesComponent } from './yourbicycles.component';

describe('YourbicyclesComponent', () => {
  let component: YourbicyclesComponent;
  let fixture: ComponentFixture<YourbicyclesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YourbicyclesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YourbicyclesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
