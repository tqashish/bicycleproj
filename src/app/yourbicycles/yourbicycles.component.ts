import { Component, OnInit } from '@angular/core';
import { RegisrService } from '../regisr.service';

@Component({
  selector: 'app-yourbicycles',
  templateUrl: './yourbicycles.component.html',
  styleUrls: ['./yourbicycles.component.css']
})
export class YourbicyclesComponent implements OnInit {

  constructor(public z:RegisrService) { }

cycle:any[]=[];

  ngOnInit() {

this.fetchdata();

  }
obj:any={};
  delete(i)
  {
    this.obj.modelno=this.cycle[i].modelno;
    console.log(this.obj.modelno);

    this.z.delete(this.obj).subscribe(b=>
      {
        alert("deleted successfully")
        this.fetchdata();
      });
  }
  fetchdata(){
    this.z.viewbicycle().subscribe((b:any)=>this.cycle=b);
  }
}
